/* lp-test.c - memory-mapped database tester/toy */
/*
 * Copyright 2011-2018 Howard Chu, Symas Corp.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted only as authorized by the OpenLDAP
 * Public License.
 *
 * A copy of this license is available in the file LICENSE in the
 * top-level directory of the distribution or, alternatively, at
 * <http://www.OpenLDAP.org/license.html>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "lmdb.h"


/*----------------------------------------------------------------------------
  Sort strings in reverse order
 */
int cmpReverseStr(const MDB_val *a, const MDB_val *b)
  {
  return -strcmp(a->mv_data, b->mv_data);
  }

/*----------------------------------------------------------------------------*/
void dropDB(MDB_env *env, char *dbName)
  {
  MDB_txn *txn;
  MDB_dbi dbi;
  int res;

  if (dbName)
    printf("Drop DB %s\n", dbName);
  else
    puts("Drop unamed DB");
  mdb_txn_begin(env, NULL, 0, &txn);
  mdb_dbi_open(txn, dbName, 0, &dbi);
  res = mdb_drop(txn, dbi, 0);
  if (res)
    printf("Error %d : %s\n", res, mdb_strerror(res));
  mdb_txn_commit(txn);
  // NEVER call mdb_txn_commit() after mdb_drop() -> SIGSEV
  //mdb_txn_commit(txn);
  }

/*----------------------------------------------------------------------------*/
int insertName(MDB_txn *txn, MDB_dbi dbi, char *name, char *data)
  {
  MDB_val key, val;
  int res;

  key.mv_size = strlen(name)+1;
  key.mv_data = name;
  val.mv_size = strlen(data)+1;
  val.mv_data = data;
  res = mdb_put(txn, dbi, &key, &val, 0);
  if (res)
    printf("%s\n", mdb_strerror(res));
  return res;
  }


/*----------------------------------------------------------------------------*/
void fillNames(MDB_env *env, char *dbName)
  {
  MDB_txn *txn;
  MDB_dbi dbi;
  int res;

  printf("Fill names in DB %s\n", dbName);
  res = mdb_txn_begin(env, NULL, 0, &txn);
  mdb_dbi_open(txn, dbName, MDB_CREATE, &dbi);
  mdb_set_compare(txn, dbi, cmpReverseStr);

  insertName(txn, dbi, "Bo", "Derek");
  insertName(txn, dbi, "Carole", "Gaessler");
  insertName(txn, dbi, "Virna", "Sachi");
  insertName(txn, dbi, "Sandrine", "Quetier");
  insertName(txn, dbi, "Fabienne", "Amiach");
  insertName(txn, dbi, "Virginie", "Efira");
  insertName(txn, dbi, "Valérie", "Payet");

  res = mdb_txn_commit(txn);
  if (res)
    printf("\tError: %s\n", mdb_strerror(res));
  }

/*----------------------------------------------------------------------------
 * If inserted numbers is too high -> Error MDB_MAP_FULL
 */
void fillNumbers(MDB_env *env, char *dbName, unsigned start, unsigned count)
  {
  MDB_val key, val;
  MDB_txn *txn;
  MDB_dbi dbi;
  int res;
  char buffers[10];

  if (count+start > 999999)
    return;
  printf("Fill numbers in DB %s\n", dbName);
  res = mdb_txn_begin(env, NULL, 0, &txn);
  mdb_dbi_open(txn, dbName, MDB_CREATE, &dbi);
  mdb_set_compare(txn, dbi, cmpReverseStr);

  while (count && !res)
    {
    sprintf(buffers, "%06d", start++);
    key.mv_size = strlen(buffers)+1;
    key.mv_data = buffers;
    val.mv_size = strlen(buffers)+1;
    val.mv_data = buffers;
    res = mdb_put(txn, dbi, &key, &val, 0);
    count--;
    }

  res = mdb_txn_commit(txn);
  if (res)
    printf("\tError: %s\n", mdb_strerror(res));
  }

/*----------------------------------------------------------------------------
 * Transaction stress. Can insert millions of numbers.
 */
void fillNumbers2(MDB_env *env, char *dbName, unsigned start, unsigned count)
  {
  MDB_val key, val;
  MDB_txn *txn;
  MDB_dbi dbi;
  int res;
  char buffers[10];

  if (count+start > 999999)
    return;
  printf("Fill numbers in DB %s\n", dbName);

  while (count && !res)
    {
    res = mdb_txn_begin(env, NULL, 0, &txn);
    mdb_dbi_open(txn, dbName, MDB_CREATE, &dbi);
    mdb_set_compare(txn, dbi, cmpReverseStr);
    sprintf(buffers, "%06d", start++);
    key.mv_size = strlen(buffers)+1;
    key.mv_data = buffers;
    val.mv_size = strlen(buffers)+1;
    val.mv_data = buffers;
    res = mdb_put(txn, dbi, &key, &val, 0);
    res = mdb_txn_commit(txn);
    count--;
    }

  if (res)
    printf("\tError: %s\n", mdb_strerror(res));
  }

/*----------------------------------------------------------------------------*/
void dumpEntries(MDB_env *env, char *dbName)
  {
  MDB_val key, data;
  MDB_txn *txn;
  MDB_dbi dbi;
  MDB_cursor *cursor;
  int res;

  if (dbName)
    printf("Dump entries in db %s :\n", dbName);
  else
    puts("Dump database names :");
  mdb_txn_begin(env, NULL, 0, &txn);
  res = mdb_dbi_open(txn, dbName, MDB_CREATE, &dbi);
  if (res == MDB_NOTFOUND)
    {
    printf("\tError: Database %s not found\n", dbName);
    mdb_txn_abort(txn);
    return;
    }
  res = mdb_cursor_open(txn, dbi, &cursor);
  if (!res)
    {
    while (mdb_cursor_get(cursor, &key, &data, MDB_NEXT) == 0)
      printf("\tkey: %s, data: %s\n", key.mv_data, data.mv_data);
    mdb_cursor_close(cursor);
    }
  else
    printf("Error %d: opening cursor\n", res);
  mdb_txn_abort(txn);
  }

/*----------------------------------------------------------------------------*/
void searchKey(MDB_env *env, char *dbName, char *name)
  {
  MDB_val key, data;
  MDB_txn *txn;
  MDB_dbi dbi;
  int res;

  mdb_txn_begin(env, NULL, 0, &txn);
  mdb_dbi_open(txn, dbName, 0, &dbi);
  mdb_set_compare(txn, dbi, cmpReverseStr);
  key.mv_data = name;
  key.mv_size = strlen(key.mv_data)+1;
  res = mdb_get(txn, dbi, &key, &data);
  if (!res)
    printf("Search \"%s\" : found \"%s\"\n", key.mv_data, data.mv_data);
  mdb_txn_abort(txn);
  }

/*----------------------------------------------------------------------------*/
void deleteKey(MDB_env *env, char *dbName, char *name)
  {
  MDB_val key, data;
  MDB_txn *txn;
  MDB_dbi dbi;
  int res;

  mdb_txn_begin(env, NULL, 0, &txn);
  mdb_dbi_open(txn, dbName, 0, &dbi);
  mdb_set_compare(txn, dbi, cmpReverseStr);
  key.mv_data = name;
  key.mv_size = strlen(key.mv_data)+1;
  printf("Delete %s\n", key.mv_data);
  res = mdb_del(txn, dbi, &key, &data);
  if (res)
    printf("Error %d deleting %s\n", res, key.mv_data);
  mdb_txn_commit(txn);
  mdb_dbi_close(env, dbi);
}

/*----------------------------------------------------------------------------*/
int countEntries(MDB_env *env)
  {
  MDB_stat stats;

  mdb_env_stat(env, &stats);
  return stats.ms_entries;
  }

/*----------------------------------------------------------------------------*/
int main(int argc,char * argv[])
  {
  int res;
  MDB_env *env;
  MDB_dbi dbi;
  MDB_val key, data;
  MDB_txn *txn;
  MDB_stat mst;
  MDB_cursor *cursor;
  MDB_cursor_op op;
  int count;
  char *dbName1 = "laurent";
  char *dbName2 = "virna";

  mdb_env_create(&env);
  mdb_env_set_maxdbs(env, 10);
  mdb_env_set_maxreaders(env, 1);
  mdb_env_set_mapsize(env, 10485760);
  mdb_env_open(env, "./testdb", MDB_FIXEDMAP | MDB_NOSUBDIR /*|MDB_NOSYNC*/, 0664);

  dropDB(env, dbName1);
  fillNames(env, dbName1);
  dropDB(env, dbName1);
  fillNumbers2(env, dbName1, 5, 400000);
  printf("%d entries found in DB :\n", countEntries(env));
  dumpEntries(env, NULL);
  dumpEntries(env, dbName1);
  fillNames(env, dbName1);
  dumpEntries(env, dbName1);
  searchKey(env, dbName1, "Carole");
  dumpEntries(env, dbName1);
  mdb_env_close(env);
  return 0;
  }

