/*
 * File:   test-lmdb-threads.c
 * Author: Laurent
 * Created on 11 novembre 2018
 *
 * This little program is a test case to learn how to use LMDB database with
 * threads. The LMDB documentation is not very clear on some points.
 * It starts hundreds threads to try thousands read/write concurrent access to a
 * single database.
 * There is only one databse (one table in the usual db terminology) populated
 * with one record for each thread.
 * This is a two stages test. Each thread starts a loop where it tries stage 1
 * test and then stage 2 and do it again until the end of the loop.
 *
 * Stage 1
 * At this stage there is a concurrent access to the DB but not to records. Each
 * thread try to access its own record with the thread number as key. The value
 * associated to the key is also an integer (unsigned) initialized to 0.
 * Each thread starts a loop ITER times which read its record and increment its
 * value. The record of each thread is not shared across other threads.
 *
 * Stage 2
 * At this stage there is a read/write concurrent access to a unique record shared
 * by all threads. Key as value -1. The value associated to this shared record is
 * also an integer (unisgned) initialized to 0. Within the stage 1 loop each
 * thread increments this value.
 *
 * Final
 * At the end of these two stages if all test was done well the value attributes
 * of all records must be ITER except for -1 key record which must be ITER*thread_numbers.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "lmdb.h"

#define FILENAME  "./test.db"

/* Number of threads */
#define NUM_THREADS 100

/* Number of iteration per threads */
#define ITER 1000


/* Global variables */
MDB_env *env;

// Ifdef display is verbose
//#define DEBUG


/*----------------------------------------------------------------------------
 * This function reads from database record with key=-1 : it increments its
 * value and store it in the db. Record with key=-1 is shared by all threads.
 * There is a concurrent access between threads to get this ressource.
 */
void incrSharedEntry()
  {
  MDB_txn *txn;
  MDB_dbi dbi;
  MDB_val key, val;
  int res;
  int k = -1;
  unsigned count;

  res = mdb_txn_begin(env, NULL, 0, &txn);
  if (res)
    {
    printf("%s\n", mdb_strerror(res));
    return;
    }
  res = mdb_dbi_open(txn, NULL, MDB_CREATE | MDB_INTEGERKEY, &dbi);
  if (res)
    {
    printf("%s\n", mdb_strerror(res));
    mdb_txn_abort(txn);
    return;
    }
  key.mv_size = sizeof(k);
  key.mv_data = &k;
  res = mdb_get(txn, dbi, &key, &val);
  if (res)
    {
    printf("%s\n", mdb_strerror(res));
    mdb_txn_abort(txn);
    return;
    }
  count = *(unsigned *) (val.mv_data);
  count++;
  val.mv_size = sizeof(count);
  val.mv_data = &count;
  res = mdb_put(txn, dbi, &key, &val, 0);
  if (res)
    {
    printf("%s\n", mdb_strerror(res));
    mdb_txn_abort(txn);
    return;
    }
  res = mdb_txn_commit(txn);
  }


/*----------------------------------------------------------------------------
 * This function is started by each thread. It starts a loop of ITER iterations
 * to :
 *   1/ Read from the database record with key=-1 : it increments its value and
 *      store it in the db. Record with key=-1 is shared by all threads. There is
 *      a concurrent access between threads to get this ressource.
 *   2/ Read from the database record with key=id : it increments its value and
 *      store it in the db. Record with key=id is dedicated to this thread and
 *      is not shared with others. There is no concurrent access between threads
 *      to get this ressource.
 *
 * Param :
 *   - id : Number of the thread starting at 0
 */
void *dbAccess(void *id)
  {
  MDB_txn *txn;
  MDB_dbi dbi;
  MDB_val key, val;
  int res;
  int tid = *(int *)id;
  unsigned long count  = 0;

  printf("Thread #%u started\n", tid);
  // Create database entry of the thread
  res = mdb_txn_begin(env, NULL, 0, &txn);
  if (res)
    printf("%s\n", mdb_strerror(res));
  else
    {
    res = mdb_dbi_open(txn, NULL, MDB_CREATE | MDB_INTEGERKEY, &dbi);
    if (res)
      printf("%s\n", mdb_strerror(res));
    key.mv_size = sizeof(tid);
    key.mv_data = &tid;
    val.mv_size = sizeof(count);
    val.mv_data = &count;
    res = mdb_put(txn, dbi, &key, &val, 0);
    if (res)
      printf("%s\n", mdb_strerror(res));

    res = mdb_txn_commit(txn);
    if (res)
      printf("\tError: %s\n", mdb_strerror(res));

    // Update database entry of the thread
    for (int i = 0; i < ITER; i++)
      {
#ifdef DEBUG
      printf("\tthread #%d, iter #%d\n", tid, i);
#endif
      incrSharedEntry();
      res = mdb_txn_begin(env, NULL, 0, &txn);
      res = mdb_dbi_open(txn, NULL, MDB_INTEGERKEY, &dbi);

      key.mv_size = sizeof(tid);
      key.mv_data = &tid;
      res = mdb_get(txn, dbi, &key, &val);
      if (!res)
        {
        count = *(int *) (val.mv_data);
        if (i != count)
          printf("\tthread #%d : ERROR waiting %d, found %d", tid, i, count);
        count++;
        val.mv_size = sizeof(count);
        val.mv_data = &count;
        res = mdb_put(txn, dbi, &key, &val, 0);
        if (res)
          printf("thread #%d : %s\n", tid, mdb_strerror(res));
        }
      res = mdb_txn_commit(txn);
      }
    }

  printf("Thread #%u ended, %u read/write\n", tid, ITER);
#if NUM_THREADS>1
  pthread_exit(NULL);
#endif
  }


/*----------------------------------------------------------------------------
 * Check values in the DB to see if all threads ran fine. Must be called when all
 * threads are ended.
*/
void checkDB()
  {
  MDB_txn *txn;
  MDB_dbi dbi;
  MDB_val key, val;
  int res;
  int k;
  unsigned count;
  unsigned errors = 0;

  puts("DB check started");
  res = mdb_txn_begin(env, NULL, 0, &txn);
  if (res)
    {
    printf("%s\n", mdb_strerror(res));
    mdb_txn_abort(txn);
    return;
    }
  res = mdb_dbi_open(txn, NULL, MDB_INTEGERKEY, &dbi);
  if (res)
    printf("%s\n", mdb_strerror(res));
  k = -1;
  key.mv_size = sizeof(k);
  key.mv_data = &k;
  res = mdb_get(txn, dbi, &key, &val);
  if (res)
    {
    printf("%s\n", mdb_strerror(res));
    mdb_txn_abort(txn);
    return;
    }
  count = *(unsigned *)val.mv_data;
  if (count == NUM_THREADS*ITER)
    {
#ifdef DEBUG
    printf("\tShared DB entry (key=-1) has expected value (%u)\n", count);
#endif
    }
  else
    {
    errors++;
#ifdef DEBUG
    printf("\tERROR: Shared DB entry (key=-1) is %u instead of (%u)\n", count, NUM_THREADS*ITER);
#endif
    }

  // Read & check database entries of every threads
  for (int i = 0; i < NUM_THREADS; i++)
    {
    k = i;
    key.mv_size = sizeof(k);
    key.mv_data = &k;
    res = mdb_get(txn, dbi, &key, &val);
    if (!res)
      {
      count = *(int *) (val.mv_data);
      if (count == ITER)
        {
#ifdef DEBUG
        printf("\tthread #%d : DB entry is ok (%u).\n", i, count);
#endif
        }
      else
        {
        errors++;
#ifdef DEBUG
        printf("\tthread #%d : DB entry is NOK, got %d but expected %d\n", i, count, ITER);
#endif
        }
      }
    }

  mdb_txn_abort(txn);
  if (errors)
    printf("%u errors found in database values\n", errors);
  else
    puts("No error found in database values");
  }


/*----------------------------------------------------------------------------*/
int main(int argc,char * argv[])
  {
  int res;
  MDB_dbi dbi;
  MDB_val key, val;
  MDB_txn *txn;
  int count = -1;
  unsigned long val2 = 0;
  pthread_t threads[NUM_THREADS];
  unsigned thdId[NUM_THREADS];

  unlink(FILENAME);
  mdb_env_create(&env);
  mdb_env_set_maxdbs(env, 10);
  mdb_env_set_maxreaders(env, NUM_THREADS+1);
  mdb_env_set_mapsize(env, 10485760);
  mdb_env_open(env, FILENAME, MDB_NOSUBDIR | MDB_NOTLS |MDB_NOSYNC, 0664);

  // Create record for all threads
  mdb_txn_begin(env, NULL, 0, &txn);
  mdb_dbi_open(txn, NULL, MDB_CREATE | MDB_INTEGERKEY, &dbi);
  key.mv_size = sizeof(count);
  key.mv_data = &count;
  val.mv_size = sizeof(val2);
  val.mv_data = &val2;
  res = mdb_put(txn, dbi, &key, &val, 0);
  if (res)
    printf("%s\n", mdb_strerror(res));
  res = mdb_txn_commit(txn);
  if (res)
    printf("\tError: %s\n", mdb_strerror(res));

#if NUM_THREADS==1
  int id = 0;
  puts("Unthreaded run");
  dbAccess(&id);
#else
  // Creation of threads
  for (int t = 0; t < NUM_THREADS; t++)
    {
    thdId[t] = t;
    res = pthread_create(&threads[t], NULL, dbAccess, &thdId[t]);
    if (res)
      {
      printf("ERROR; return code from pthread_create() is %d\n", res);
      exit(-1);
      }
    }

  // Waiting for all threads end
  for (int t = 0; t < NUM_THREADS; t++)
    {
    res = pthread_join(threads[t], NULL);
    if (res)
      {
      printf("ERROR; return code from pthread_join() is %d\n", res);
      exit(-1);
      }
    }
#endif
  checkDB();

  // All threads are ended, we can close DB
  mdb_env_close(env);
  printf("%d threads without big lock protection\n", NUM_THREADS);
  printf("%lu read/write\n", ITER*2*NUM_THREADS+NUM_THREADS);
#if NUM_THREADS==1
  pthread_exit(NULL);
#endif
  return 0;
  }

