This little program is a test to learn how to use LMDB database with threads.
The [LMDB documentation](http://www.lmdb.tech/doc/) is not very clear on some points.
It starts hundred threads to try read/write concurrent access to a single database.
There is only one database (one table in the usual db terminology) populated
with one record for each thread.
This is a two stages test. Each thread starts a loop where it tries stage 1 test
and then stage 2 and do it again until the end of the loop.
If NUM_THREADS=1 the program start the test loop without threading.

## Stage 1
At this stage there is a concurrent access to the DB but not to records. Each thread
try to access its own record with the thread number as key. The value associated
to the key is also an integer (unsigned) initialized to 0.
Each thread starts a loop ITER times which read its record, increment its value
and write it to the DB. The record of each thread is not shared across threads.

## Stage 2
At this stage there is a read/write concurrent access to a unique record (key=-1)
shared by all threads. The value associated to this shared record si also an integer
(unsigned) initialized to 0.
At this stage each thread reads the record value (key=-1), increment it and then
write the record to the DB.

## Final
At the end of these two stages if all test ended well the value attributes of
all records must be ITER except for -1 key record which must be ITER*thread_numbers.
The final stage compare read values to expected values and can conclude if everything
is fine.

